// const sum = function(a, b) {
//   return a + b;
// }
// const multiply = function(a, b) {
//   setTimeout(() => {
//     return a * b;
//   }, 1000);
//   // return undefined;
// }

// const print = function(callbacks, a, b) {
//   let result = 0;
//   callbacks.forEach(fn => {
//     result = result + fn(a, b);
//   });
//   console.log(result);
// }

// print([sum, multiply], 2, 3);


const multiply = function (a, b) {
  //  return Promise((resolve, reject) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(a * b);
    }, 1000);
  })
}

// parentesis opcionales con una variable
// .then(a => console.log(a));
// en una linea sin corchetes, hace return
const sum = (a, b) => new Promise(resolve => resolve(a + b));

multiply(2,3)
  .then((value) => {
    console.log('multiply', value);
    return sum(value, 5);
  })
  .then(value => {
    console.log(value);
    return 6;
  })
  .then(value => {
    console.log('value2', value);
  })
  .catch((error) => console.log(error));
// console.log(multiply(2, 3));

// function sum2(a, b) {
//   return a + b;
// }

// sum2(7, 9).then ==> error


// sum(2, 3) y al resultado sumar 5, y a todo eso multiplicalo por 2
// ((2 + 3) + 5) * ( 2 + 3) = 50

sum(2, 3)
  .then((result) => {
    return sum(result, 5);
  })
  .then((resultSum) => {
    return sum(2, 3)
      .then((result) => {
        return multiply(resultSum, result);
      })
  })
  .then(console.log);

( (2 + 3) + 5)
const part1 = sum(2, 3)
  .then((result) => {
    return sum(result, 5);
  });
console.log(part1);
(2 + 3);
const part2 = sum(2, 3);

Promise.all([part1, part2])
  .then((values) => {
    // return multiply(values[0], values[1]);
    return multiply(...values);
  })
  .then(console.log);

const arr1 = [1, 2];
const arr2 = [3, 4];

console.log([...arr1, ...arr2]);

async function asyncExample() {
  const result = await sum(2, 3);
  const result2 = sum(result, 5);
  const result3 = sum(2, 3);
  const values = await Promise.all([result2, result3]);
    // .then(async (values) => {
  const value = await multiply(...values);
  return `value: ${value}`;
        // return multiply(...values)
        //   .then((value) => {
        //     return `value: ${value}`;
        //   })
    // })
}

asyncExample()
  .then((value)  => console.log('async', value))