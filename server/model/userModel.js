const users = [
    {
      id: 1,
      name: 'Juan',
    },
    {
      id: 2,
      name: 'Maria',
    },
];

async function getUser(id) {
  return new Promise((resolve) => {
    setTimeout(() => {
      const user = users.find(element => element.id === id);
      resolve(user);
    }, 500);
  })
}

module.exports = {
  getUser,
};