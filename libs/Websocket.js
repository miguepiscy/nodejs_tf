const Websocket = require('ws');
let instance;
class WSocket {
  constructor(server, path) {
    const wss = new Websocket.Server({ server, path });
    this.open(wss)
      .then((ws) => {
        console.log('open', ws)
        this.ws = ws;
      });
    instance = this;
    // wss.emit('connection', 'test');
  }

  static getInstance() {
    return instance;
  }

  // evento.on('verbo', callback) => escucha todos los eventos
  // evento.once('verbo', callback) => escucha el primer evento
  // evento.emit('verbo', data)
  async open(wss) {
    return new Promise((resolve) => {
      wss.once('connection', (ws) => {
         console.log('resolve');
        resolve(ws);
      });
    });
  }

  // onMessage()

  publish(mail) {
    if (!this.ws) {
      console.log('no client');
    } else {
      this.ws.send(mail)
    }
  }
}

// module.exports = function(server, path) {
//   if (!instance) {
//     instance = new WSocket(server, path);
//   }
//   return instance;
// }

module.exports = WSocket;