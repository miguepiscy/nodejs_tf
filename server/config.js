module.exports = {
  redisHost: '127.0.0.1',
  redisPort: process.env.REDIS_PORT || 6379,
}