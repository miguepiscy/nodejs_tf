const amqp = require('amqplib');
const { EventEmitter } = require('events');
class Amqp extends EventEmitter {
  constructor(queue) {
    super();
    this.queue = queue;
    this.open(queue);
    this.ready = false;
  }

  open(queue, durable = true) {
    const urlAmqp = process.env.amqp || 'amqp://localhost';
    amqp.connect(urlAmqp)
      .then((conn) => {
        // conn.createConfirmChannel();
        return conn.createChannel();
      })
      .then((ch) => {
        const assertQueue = ch.assertQueue(queue, { durable });
        // ch.assertExchange('expreg', 'topic', { durable });
        return assertQueue
          .then(() => {
            this.channel = ch;
            console.log('ready');
            this.ready = true;
            this.emit('ready', ch);
          });
      })
      .catch((error) => {
        this.emit('lost', error);
      });
  }

  async waitForReady() {
    if (this.ready) {
      return true;
    }
    return new Promise((resolve) => {
      this.once('ready', () => {
        console.log('ready');
        resolve()
      })
    })
  }

  publish(msg) {
    const messageString = JSON.stringify(msg);
    const content = Buffer.from(messageString);
    return this.channel.sendToQueue(this.queue, content, { persistent: true });
  }
  consume(promise, notSendAck) {
    return this.channel.consume(this.queue, msg => promise(msg)
      .then(() => {
        if (!notSendAck) {
          return this.channel.ack(msg);
        }
        return undefined;
      }), { noAck: false });
  }

  sendAck(msg) {
    return this.channel.ack(msg);
  }

  sendNack(msg, allUpTo, requeue) {
    return this.channel.nack(msg, allUpTo, requeue);
  }


}

module.exports = Amqp;