function main() {
  A();
  setTimeout(() => {
    B();
  }, 1000);

  // setTimeout(function() {
  //   B();
  // }, 1000)
  C();
}

function A() {
  let objeto = { a: 1};
  console.log(objeto);
  objeto = {a: 3};
  objeto = cambiaobjeto(objeto);
}

function cambiaobjeto(objeto) {
  const objeto2 = objeto;
  objeto2.a = 2;
  return objeto2;
}

function B() {
  console.log('B')
}

function C() {
  console.log('C')
}

main();