const Amqp = require('../libs/Amqp');

const amqp = new Amqp('test');

amqp.waitForReady()
  .then(() => {
    for (let index = 0; index < 30; index++) {
      amqp.publish({ a: index });
    }
    amqp.consume(async (msg) => {
      const json = JSON.parse(msg.content.toString());
      console.log(json);
      amqp.sendAck(msg);
    }, true);
  })