const { getPackageVersion } = require('./packages');
describe('packages', () => {
  describe('getPackageVersion', () => {
    it('version of lodash should be 4.17.15', () => {
      return getPackageVersion('lodash')
        .then((version) => {
          expect(version).toBe('4.17.15');
        });
    });
  });
})