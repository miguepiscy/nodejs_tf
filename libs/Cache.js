const redis = require('redis');
const config = require('../server/config');
const util = require('util');
class Cache {
  constructor() {
    this.redisClient = new redis.createClient(config.redisPort, config.redisHost);
  }

  async get(key) {
    const get = util.promisify(this.redisClient.get).bind(this.redisClient);
    return get(key)
      .then(JSON.parse);
  }

  async set(key, value) {
    // opcion promise
    // return this.get(key)
    //   .then((element) => {
    //     if (!element) {
    //       this.redisClient.set(key, JSON.stringify({ key, value }));
    //     }
    //   });
    // opcion await
    const element = await this.get(key);
    if (!element) {
      this.redisClient.set(key, JSON.stringify({ key, value }));
    }
  }

  async keys(expreg) {
    const keys = util.promisify(this.redisClient.keys).bind(this.redisClient);
    return keys(expreg);
  }

  subscribe(ch) {
    this.redisClient.on('message', (channel, message) => {
      console.log(`Message: ${message} on channel ${channel}`);
    });
    this.redisClient.subscribe(ch);
  }

  publish(ch, message) {
    this.redisClient.publish(ch, message);
  }
}
// freeze
const instance = new Cache();
module.exports = {
  cache: Object.freeze(instance),
  Cache,
};
