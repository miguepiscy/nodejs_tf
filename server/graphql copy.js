const { ApolloServer, gql, SchemaDirectiveVisitor } = require('apollo-server-express');
const { typeDefs: typesUser, resolvers: resolvesUser } = require('./schemas/userSchema');
const { verify } = require('../libs/jwt');
const Websocket = require('../libs/Websocket');
const { cache } = require('../libs/Cache');
// const { merge } = require('lodash');
// const _ = require('lodash');
// _.merge()
const merge = require('lodash/merge');
class UpperCaseDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    // const resolve = field.resolve;
    const { resolve } = field;
    field.resolve = async function (...args) {
      const result = await resolve.apply(this, args);
      console.log('upperdirective', result);
      return result;
    }
  }
}
const typeDefs = gql`
  directive @upper on FIELD_DEFINITION
  type Hello {
    text: String
    name: String
  }

  type Query {
    hello(name: String): Hello
    hi: String @upper
  }

  type Mutation {
    sendMail(mail: String): String
  }
`;

const resolvers = {
  Query: {
    hello: (_, { name }) => ({
        text: `Hello ${name}`,
        name,
    }),
    hi: () => 'Hello',
  },
  Mutation: {
    // sendMail: (_, { mail }) => Websocket.getInstance().publish(mail),
    sendMail: (_, { mail }) => cache.publish('test', mail),
  }
};

// const server = new ApolloServer({ typeDefs, resolvers });
// const mergeResolves = {...resolvers, ...resolvesUser};
// const mergeResolves = Object.assign( resolvers, resolvesUser);
// const Query = { ...resolvers.Query, ...resolvesUser.Query };
// const Mutation = { ...resolvers.Mutation, ...resolvesUser.Mutation };

const mergeResolves = merge(resolvers, resolvesUser);

const options = {
  typeDefs: [ typeDefs, typesUser],
  resolvers: mergeResolves,
  schemaDirectives: {
    upper: UpperCaseDirective
  },
  context(context) {
    return {
      requireAuth() {
        console.log('require auth');
      },
      async getLogedUserId() {
        const token = context.req.headers.token;
        const { userId } = await verify(token); 
        return userId
      }
    }
  }
};

if (process.env.NODE_ENV !== 'production') {
  options.introspection = true;
  options.playground = {
    endpoint: 'play',
  };
}

const server = new ApolloServer(options)

module.exports = server;