function Circle(x, y) {
  this.x = x;
  this.y = y;
  // this.point = function point() {
  //   console.log(`${this.x}, ${this.y}`);
  // }
  this.point = () => {
    console.log(`${this.x}, ${this.y}`);
  }
}

const circle = new Circle(2, 3);
circle.point();
// 2, 3
setTimeout(circle.point, 1000);
