const ERRORS = {
  INTERNAL: 500,
  NOT_FOUND: 404,
};
class ExtendableError extends Error {
  constructor(message, status) {
    super(message);
    this.message = message; // revisar github
    this.status = status;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.name);
  }
}

class APIError extends ExtendableError {
  constructor(message, status = ERRORS.INTERNAL) {
    super(message, status);
  }
}


module.exports = {
  APIError,
  ERRORS,
};