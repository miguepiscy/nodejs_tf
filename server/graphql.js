const { ApolloServer, gql, SchemaDirectiveVisitor } = require('apollo-server-express');
const { typeDefs: typesUser, resolvers: resolvesUser } = require('./schemas/userSchema');
const { verify } = require('../libs/jwt');
const Websocket = require('../libs/Websocket');
const { cache } = require('../libs/Cache');
const { defaultFieldResolver, GraphQLScalarType, Kind } = require("graphql");
// const { merge } = require('lodash');
// const _ = require('lodash');
// _.merge()
const merge = require('lodash/merge');
class UpperCaseDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    // const resolve = field.resolve;
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async function (...args) {
      const result = await resolve.apply(this, args);
      if (typeof result === 'string') {
        // return result.toUpperCase();
        return new Promise(resolve => resolve(result.toUpperCase()));
      }
      return result;
    }
  }
}

function sum(a = 0, b = 0) {
  return a + b;
}
console.log(sum());
console.log(sum(1));
console.log(sum(1,2));

const typeDefs = gql`
  directive @upper on FIELD_DEFINITION
  scalar Coordinates
  scalar Date

  type Hello {
    text: String  @upper
    name: String
  }

  type Query {
    hello(name: String): Hello
    hi: String @upper
    location: Coordinates
    currentDate: Date
    getDate(date: Date) : String
  }

  type Mutation {
    sendMail(mail: String): String
  }
`;
// const coordinates = [0, 2];
const coordPolygon = [[0,2], [0,3]];
// const multy = [[[0, 2], [0, 3]]];

const resolvers = {
  Query: {
    hello: (_, { name }) => ({
      text: `Hello ${name}`,
      name,
    }),
    hi: () => 'Hello',
    location: () => {
      return coordPolygon;
    },
    currentDate: () => new Date(),
    getDate: (_, { date }) => {
      console.log('getDate', typeof date);
      return 'ok'
    }
  },
  Mutation: {
    // sendMail: (_, { mail }) => Websocket.getInstance().publish(mail),
    sendMail: (_, { mail }) => cache.publish('test', mail),
  },
  Coordinates: new GraphQLScalarType({
    name: 'Coordinates',
    description: 'Array of coordinates for all dimensions',
    parseValue(value) {
      return value;
    },
    serialize(value) {
      return value;
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.ARRAY) {
        return ast.value;
      }

      return null;
    },
  }),
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date',
    parseValue(value) {
      console.log('parseValue', value);
      return new Date(value); // value from the client
    },
    serialize(value) {
      return value.getTime(); // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        console.log(ast.kind, ast.value);
        return parseInt(ast.value, 10); // ast value is always in string format
      }
      return null;
    },
  }),
};

// const server = new ApolloServer({ typeDefs, resolvers });
// const mergeResolves = {...resolvers, ...resolvesUser};
// const mergeResolves = Object.assign( resolvers, resolvesUser);
// const Query = { ...resolvers.Query, ...resolvesUser.Query };
// const Mutation = { ...resolvers.Mutation, ...resolvesUser.Mutation };

const mergeResolves = merge(resolvers, resolvesUser);

const options = {
  typeDefs: [typeDefs, typesUser],
  resolvers: mergeResolves,
  schemaDirectives: {
    upper: UpperCaseDirective
  },
  context(context) {
    return {
      requireAuth() {
        console.log('require auth');
      },
      async getLogedUserId() {
        const token = context.req.headers.token;
        const { userId } = await verify(token);
        return userId
      }
    }
  }
};

if (process.env.NODE_ENV !== 'production') {
  options.introspection = true;
  options.playground = {
    endpoint: 'play',
  };
}

const server = new ApolloServer(options)

module.exports = server;