const posts = [
  {
    id: 1,
    title: 'titulo1',
    userId: 1,
  },
  {
    id: 2,
    title: 'titulo2',
    userId: 2,
  },
];

async function getPost(id) {
  return posts.find(element => element.id === id);
}

module.exports = {
  getPost,
};