const { gql } = require('apollo-server-express');
const { getUser } = require('../model/userModel');
const { getPost } = require('../model/postModel');
const { sign } = require('../../libs/jwt');
const { APIError,ERRORS } = require('../../libs/APIError')
const typeDefs = gql`
  type User {
    id: Int!
    name: String!
    timestamp: Float
  }
  type Post {
    id: Int!
    title: String!
    author: User
  }
  extend type Query {
    user(id: Int): User
    post(id: Int): Post
    me: User
  }
  extend type Mutation {
    login(userId: Int) : String
    saveLike(postId: Int) : String
  }
`;

const resolveUser = (_, { id }) => {
  const timestamp = new Date().getTime();
  // const user = getUser(id);
  // user.timestamp = timestamp;
  // return user;
  return getUser(id)
    .then(user => {
      user.timestamp = timestamp;
      return user;
    })
}

const resolvers = {
  Query: {
    user: resolveUser,
    me: async (_, input, ctx) => {
      const userId = await ctx.getLogedUserId();
      return resolveUser(null, { id: parseInt(userId) }, ctx)
    },
    post: (_, { id }) => {
      return getPost(id);
    }
  },
  Post: {
    author(parent,_, ctx) {
      const userId = parent.userId;
      // return getUser(userId);
      return resolveUser(null, { id: userId }, ctx)
    }
  },
  Mutation: {
    login: (_, { userId }) => {
      return getUser(userId)
        .then((user) => {
          if (!user) {
            throw new APIError('not found', ERRORS.NOT_FOUND );
          }
          const { id, name } = user;
          return sign({userId: id, name});
        })
    },
    saveLike: async (_, { postId }, ctx) => {
      const userId = await ctx.getLogedUserId();
      console.log('saveLike(userId, postId)');
    },
  }
}

module.exports = { typeDefs, resolvers };