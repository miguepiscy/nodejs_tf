
const { getListPackages, getPackageVersion } = require('../../libs/packages');
const { APIError, ERRORS } = require('../../libs/APIError');
const router = require('express').Router();
const { cache } = require('../../libs/Cache');

router.get('/', (req, res, next) => {
  getListPackages()
    .then(res.json.bind(res))
    .catch(next)
});

// router.get('/:name', middlewareCache);
router.get('/:name', middlewareCache, (req, res, next) => {
  const name = req.params.name;
  console.log(req._cache);
  if (req._cache) {
    const obj = {};
    obj[req._cache.key] = req._cache.value;
    return res.json(obj);
  }
  getPackageVersion(name)
    .then((version) => {
      const obj = {};
      obj[name] = version;
      cache.set(name, version);
      console.log('NO CACHE');
      return obj;
      // return { name, version};
    })
    .then(res.json.bind(res))
    .catch(() => {
      const error = new APIError('Not found', ERRORS.NOT_FOUND);
      next(error);
    });
  //  throw new Error('404');
});

// router.get('/perfil', () => {...});

function middlewareCache(req, res, next) {
  cache.get(req.params.name)
    .then((element) => {
      if (element) {
        // res.json...
        req._cache = element;
      }
      next();
    });
}

module.exports = router;