function* id() {
  let index = 0;
  while(index < 100) {
    yield index++;
  }
}

const genId = id();

let result = genId.next();
// setTimeout(() => {
//   console.log(result.value);
// }, 1000);
while(!result.done) {
  console.log(result.value);
  result = genId.next();
}
// const hola = 'hola';
// const hola2 = "hola";
result = genId.next();
console.log(result);