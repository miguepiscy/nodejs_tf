const fs = require('fs');
const path = require('path');
const util = require('util');
const fsReadFile = util.promisify(fs.readFile);
const fsReadDir = util.promisify(fs.readdir);

async function getPackageVersion(packageName) {
  const file = path.join(__dirname, '..', 'node_modules', packageName, 'package.json');
  return fsReadFile(file, 'utf8')
    .then((data) => {
      const json = JSON.parse(data);
      return json.version;
    });
  // return new Promise((resolve, reject) => {
  //   fs.readFile(file, (err, data) => {
  //     if (err) {
  //       reject(err);
  //     }
  //     const json = JSON.parse(data);
  //     resolve(json.version);
  //   });
  // });
}

async function getListPackages() {
  const folder = path.join(__dirname, '..', 'node_modules');
  return fsReadDir(folder)
    .then((list) => {
      const promises = [];
      list.forEach((folder) => {
        const promiseVersion = getVersion(folder)
          .then(printThen)
          // .then((version) => version ? version : '0')
          .then((version) => {
            // {name: folder, version: version }
            return { name: folder, version };
          });
        promises.push(promiseVersion);
      });
      return Promise.all(promises)
    })
    .then(cleanEmptyItems);
}

// [{nombre: version, nombre: version...}];

const cleanEmptyItems = (list) => {
  // return list.filter(element => typeof(element) === 'string');
  return list.filter(element => element.version);
};

async function getVersion(folder) {
  return getPackageVersion(folder)
    .catch(() => {});
    // .finally(() => {
    //   // codigo
    // });
  // return '4';
}

function printThen(value) {
  return value;
}

module.exports = {
  getPackageVersion,
  getListPackages,
}

//  cuidado al pasar objetos a promesas!!
// let objeto = {};
// array.forEach(objeto2 => {
//   objeto = objeto2;
//   promises.push(promise(objeto));
// });