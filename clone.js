// const server = require('./server/index');
// const server = require('./server');

const { clone, cloneDeep } = require('lodash');
const objt1 = { a: 1};

const objt2 = cloneObject(objt1);

function cloneObject(objt) {
  return Object.assign({}, objt);
}

const example = Object.assign({b:1}, {c:2}, {b:2, d:1});
console.log(example);
const { b, d } = example;
console.log(b, d);
const example2 = clone(objt1);
console.log(objt2, example2);

const subObject = { a: 1, b: { c: 1}};
console.log('original', subObject);
// const copy = Object.assign({}, subObject);
const copy = cloneDeep(subObject);
copy.b.c = 2;
copy.a = 3;

// console.log(JSON.stringify(copy, null, 2));
// const objectFromString = JSON.parse('{"a": 1}');
// console.log(objectFromString);

console.log('original2', subObject);
// 'copy: '+ copy;
console.log(`Copy: ${JSON.stringify(copy)}`);

// const cloneOther = JSON.parse(JSON.stringify(subObject));

const object = {
  a: 1,
  b: 2,
  c: 3,
};

const object2 = {
  a:1,
  b: 2
  // c: 3
};