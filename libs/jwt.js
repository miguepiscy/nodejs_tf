const jwt = require('jsonwebtoken');
const SECRET = 'secret';

async function sign({ userId, name }) {
  const token = jwt.sign({ userId, name }, SECRET);
  return token;
}

async function verify(token) {
  const user = jwt.verify(token, SECRET);
  return user;
}

module.exports = {
  sign,
  verify,
}