const express = require('express');
const app = express();
const { APIError, ERRORS } = require('../libs/APIError');
const packageController = require('./controller/packageController');
const { cache } = require('../libs/Cache');
const servergql = require('./graphql');
const http = require('http');
const Websocket = require('../libs/Websocket');

const server = http.createServer(app);

new Websocket(server, '/websocket');

// const wss = new Websocket.Server({server, path: '/websocket'});

// wss.on('connection', (ws) => {
//   ws.on('message', (message) => {
//     console.log(message);
//     ws.send('message received');
//   });
//   ws.send('Hi');
// });
// const wss = 
// const packages = require('packages');
// packages.getListPackages;
const PORT = process.env.PORT || 3000;
app.use(express.json());

app.use('/graphql', (req, res, next) => {
  console.log('capturing graphql');
  // let key = req.body.query;
  // key = key.replace(/\s/g, '');
  
  // res.originalWrite = res.write;
  // res.on('finish', function () {
  //   console.log('finish');
  //   console.log(arguments)
  //   console.log(res._gplRespone);
  // });
  // // proxy es6
  // res.write = function (gplRespone) {
  //   console.log('write', gplRespone)
  //   res._gplRespone = gplRespone;
  //   return res.originalWrite(...arguments);
  // }
  next();
});
servergql.applyMiddleware({ app, path: '/graphql'});

app.get('/hola', (req, res) => {
  res.send('hola');
})

app.get('/', (req, res, next) => {
  console.log('response');
  next();
  // next(error);
});

app.get('/', (req, res) => {
  // res.send('Hello word2');
  res.json({say: 'hello wold'})
});

app.post('/', (req, res) => {
  console.log(req.body);
  res.json(req.body);
})
// (request, response, next)
// (error, request, response, next)

app.use('/keyscache/:expreg', (req, res, next) => {
  cache.keys(req.params.expreg)
    .then(res.json.bind(res))
    .catch(next);
});

app.use('/packages', packageController);


app.use((err, req, res, next) => {
  //  err.message === 404...
  if(err instanceof APIError) {
    return res.status(err.status).json({error: err.message});
  }
  return res.status(ERRORS.INTERNAL).json({error: err.message});
});

server.listen(PORT, () => console.log(`Server listen in port: ${PORT}`));