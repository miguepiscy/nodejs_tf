const { sum } = require('./math');
describe('math', () => {
  describe('sum', () => {
    it('1 + 2 to equal 3', () => {
      expect(sum(1, 2)).toBe(3);
    });
    it('null + 2 throw error', () => {
      expect(() => sum(null, 2)).toThrow();
    });

    it('0 + 2 to equal 2', () => {
      expect(sum(0, 2)).toBe(2);
    });
  });
})